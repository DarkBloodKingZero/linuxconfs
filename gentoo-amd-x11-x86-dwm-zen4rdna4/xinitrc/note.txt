Since xinitrc is actually ".xinitrc" I had to make it visible by removing
the dot. To permanantly replace your xinitrc type this to your terminal: "mv ./xinitrc
~/.xinitrc". Note that this can only be done while being logged in as a
regular user, not root.
