# My Linux System Configs

## Description
My personal configs for Gentoo Linux, you can make suggestions and use it on your machine for any kind of purpose.

## Installation
For now, you can only manually install and edit these configs and it's up to you to make them work for your specific use case.

## Roadmap
1-Make your configs public - DONE
2-Add more variants to the repository
3-Make an executable for easy installation.


## Contributing
Do not fix typos.
Don't act like you know the best, don't contribute without giving a logical explanation for the change you made.
Since it's my personal repository, I've the final say but you can fork it if you want to make your own versions.
I've no idea how to properly organize config files, feel free to propose better and more efficent hirearchies.

## License
CC0 1.0 Universal - This work has been marked as dedicated to the public domain.
You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission.
https://creativecommons.org/publicdomain/zero/1.0

Exceptions: /gentoo-amd-x11-x86-dwm/grub/themes/darktheme/beyazseksiejder.png -  CC BY-SA 4.0 Attribution-ShareAlike 4.0 International
            /home/dark/Desktop/Miyav/git/linuxconfs/gentoo-amd-x11-x86-dwm/grub/themes/darktheme/COPYING.CC-BY-SA-3.0
